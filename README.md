# Scripts
General purpose scripts


## getdoc.py

Script which downloads & formats an online text (article, blogpost) for printing / easier reading.

Uses pandoc, BeautifulSoup and exiftool. Can produce most formats that pandoc can output to (PDF, HTML, DOCX, EPUB, ODT,
RTF, etc.), MHT (custom generator) or ZIP archive of HTML + images.

Sample usage: `getdoc.py http://some.website.com/some-awesome-article-or-blogpost -o pdf`

See the script header for installation instructions.


## io-filegen.py

Script which generates random-content files for various purposes. Known file formats:

- images (BMP, PNG, JPG, GIF)
- PE executables (32/64, DLLs, console / GUI)
- random text (ASCII, UTF-8, UTF-16)
- random binary data (including a "sparse" mode which inserts zeros to allow somewhat better compression)

Examples:

- generate a JPEG image: `io-filegen -f jpg`
- generate 10 PEs: `io-filegen.py -f exe -c 10`
- generate 3 32-bit DLLs, keep FASM sources: `io-filegen.py -f exe -ed -eb 32 -eks -c 3`
- generate a sparse random binary data file of 1-16 MB: `io-filegen.py -f bin -ms 0x100000 -Ms 0x1000000 -bs`

Image generation requires `pillow` to be installed.
PE executable generation requires `fasm` to be in the system `PATH`.
To improve generation speed use PyPy (for everything except image files).

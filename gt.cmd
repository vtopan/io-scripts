@echo off
rem Git wrapper
rem Author: Vlad Topan (vtopan/gmail)
rem Version: 0.1.3 (2020.04.01)

setlocal enabledelayedexpansion

if _%1_ == _a_ git add "%2"
if _%1_ == _ac_ git add "%2" && git commit --amend
if _%1_ == _am_ git commit --amend
if _%1_ == _b_ git branch|cat -n
if _%1_ == _c_ git commit -m "%2"
if _%1_ == _ca_ git commit --amend
if _%1_ == _carn_ git commit --amend --reset-author --no-edit
if _%1_ == _cd_ git clone --depth=1 "%2"
if _%1_ == _co_ git checkout "%2"
if _%1_ == _d_ git diff
if _%1_ == _gl_ git log --all --grep="%2"
if _%1_ == _i_ ( 
    echo === Branches ===
    git branch --color=always
    echo === Status ===
    git status -s
    echo === Log ===
    git log --oneline|head -n5
)
if _%1_ == _l_ git log --oneline --color=always
if _%1_ == _p_ git pull
if _%1_ == _plm_ git pull local master
if _%1_ == _pr_ git pull --recurse-submodules
if _%1_ == _pp_ ( git pull & git push )
if _%1_ == _P_ git push
if _%1_ == _ri_ git rebase --interactive "%2"
if _%1_ == _rc_ git rebase --continue
if _%1_ == _s_ git status -s
if _%1_ == _sc_ git stash clear
if _%1_ == _Sc_ git stash clear
if _%1_ == _S_ git stash
if _%1_ == _Sp_ git stash pop
if _%1_ == _sp_ git stash pop
rem staged changes
if _%1_ == _t_ git diff --cached
if _%1_ == _u_ git reset HEAD~
rem unpushed commits
if _%1_ == _U_ git log --branches --not --remotes
if _%1_ == _?_ git show "%2"

:end

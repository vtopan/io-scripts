#!/usr/bin/env python3
"""
CTFd automation tool (download challenges, submit flags, etc.).
Known engines: ctfd, rctf, watflag, ctfx.

Create an .auth file in the current folder with the CTF's base URL on the first line and
authentication info on the second line, depending on the backend type (take this from
either the `Authorization` or the `Cookie` header from a browser request after login):
- ctfd: `Token ...`
- rctf: `Bearer ...`
- watflag: `csrftoken=...`
- ctfx: `PHPSESSID=...`

Example .ioctf:

{
	'url': 'https://ctf.url.com/',
    'token': 'session=1-2-3-AABBCC',
    'token_header': 'Cookie',
    'headers': [
        'Csrf-Token: aabbccddeeff00112',
    ],
    'mode': 'ctfd',
}

Example .auth file:
https://my.awesome.ctf/
PHPSESSID=1234

Author: Vlad Topan (vtopan/gmail)
"""
import argparse
import ast
from html.parser import HTMLParser
import http.client
import json
import os
import pprint
import re
import ssl
import sys
import time
import urllib
from urllib.request import urlopen, Request
from urllib.parse import urljoin

import requests
NCSESSION = requests.Session()
try:
    import requests_cache
    SESSION = requests_cache.CachedSession('ioctfd.cache')
except ImportError:
    print('[!] Module requests_cache missing, caching disabled')
    SESSION = NCSESSION


__VER__ = '0.1.10.241115'

PATHS = {
    'ctfd': {
        'user_info': '/api/v1/users/me',
        'user_solves': '/api/v1/users/me/solves',
        'challenges': '/api/v1/challenges',
        'team_info': '/api/v1/teams/me',
        'team_pub_info': '/api/v1/teams/$team_id',
        'awards': '/api/v1/awards',
        'notifications': '/api/v1/notifications',
        'submissions': '/api/v1/submissions',
    },
    'rctf': {
        'challenges': '/api/v1/challs',
        'team_info': '/api/v1/users/me',
    },
    'watflag': {
        'challenges': '/api/watsup',
        'team_info': '/api/watsup',
    },
    'ctfx': {
        'challenges': '/challenges',
        'team_info': '/profile',
        'scoreboard': '/scoreboard',
    },
}
INFO = {
    'user_info': ('User info', ['id', 'name', 'email', 'place', 'score', 'banned']),
    'team_info': ('Team info', ['id', 'name', 'password', 'banned', 'score', 'globalPlace', 'divisionPlace']),
    'team_pub_info': ('Team public info', ['name', 'website', 'place', 'score']),
    'awards': ('Award info', ['id', 'name', 'email', 'place', 'score', 'banned']),
}
FIELD_ALIAS = {
    'description': ['desc',],
    'name': ['title',],
    'category': ['categories', 'tag', 'tags',],
    'categories': ['tag', 'tags',],
    'id': [],
    'points': ['score', 'value',],
    'url': [],
    'data': ['challenges', 'content',],     # top-level dict key which contains challenge data
    'meta': ['metadata',],
    'files': ['file_urls', 'attachments'],
}


def download(url, headers=None, params=None, nocache=False, method='get', **kwargs):
    use_headers = dict(OPT.headers)
    if headers:
        use_headers.update(headers)
    fun = getattr(NCSESSION if (nocache or OPT.no_cache) else SESSION, method or ('post' if params else 'get'))
    if OPT.insecure:
        kwargs['verify'] = False
    if OPT.verbose:
        print(f'[#] Downloading {url}...')
        if OPT.verbose > 1:
            print(f'[#] Headers: {use_headers}')
    r = fun(url, headers=use_headers, params=params, **kwargs)
    return r


def get_download_info(url, **kwargs):
    r = download(url, method='head', **kwargs)
    size = int(r.headers['content-length'])
    name = None
    if 'content-disposition' in r.headers:
        if (m := re.search('filename="?([^"]+)"?', r.headers['content-disposition'])):
            name = m.groups()[0]
        else:
            sys.stderr.write(f'[!] Missing filename in content-disposition: {r.headers["content-disposition"]}!\n')
    else:
        sys.stderr.write(f'[!] Missing content-disposition from response: {list(r.headers)}!\n')
    return {'size': size, 'name': name}


def get_json(url, params=None, suffix=None):
    url = PATHS[OPT.mode].get(url, url)
    if not re.search('https?://', url):
        url = OPT.url + url
    if suffix:
        url += suffix
    if '$' in url:
        field = re.search(r'\$\w+', url).group()
        value = getattr(OPT, field[1:], None)
        if not value:
            print(f'[!] Field {field} for URL {url} is not set!')
            return {'data': {}}
        url = url.replace(field, str(value))
    headers = {'Content-Type': 'application/json'}
    r = download(url, headers, params)
    try:
        jdata = r.json()
    except:
        jdata  = {}
    if r.status_code != 200:
        if r.status_code == 403:
            print(f'[!] Not authorized to access {url}!')
            return {'data': {}}
        if 'message' in jdata:
            raise ValueError(f'API call {url} failed: {jdata["message"]}')
        if r.status_code != 404:
            print(r.text)
        raise ValueError(f'Download of {url} failed: {r.status_code}')
    if OPT.verbose and OPT.verbose > 1:
        print(f'[#] Result:\n' + json.dumps(jdata))
    return jdata


def check_token():
    res = get_json('user_solves')
    if res.get('success'):
        print(f'[*] OK. You have solved {res["meta"]["count"]} challenges so far.')
    else:
        print(f'[!] Failed!')
        print(res)


def filter_fn(s):
    return re.sub(r'[^\w.,@-]+', '', s).strip('. ') or 'unknown'


def clear_tags(text):
    text = re.sub(r'<[^>]+>|&nbsp;', ' ', text)
    text = re.sub(r'[ \t]+', ' ', text)
    text = re.sub(r'[\r\n][ \r\n]+', '\n', text)
    return text.strip()


def html_to_dict(text):
    categories, challenges = {}, []
    for href, cat in re.findall(r'<a [^>]+? href="(challenges\?category=[^"]+)">([^<]+)</a>', text):
        categories[cat] = urljoin(OPT.url, href)
    for cat, url in categories.items():
        text = download(url).text
        for sec in re.split(r'<div class="card "|<div id="footer">|<div class="pre-category-name">', text):
            m = re.search(r'<a href="challenge\?id=(\d+)">([^<]+)</a>', sec)
            if not m:
                continue
            chal = {'name': m[2], 'id': m[1], 'files': [], 'category': cat, 'solves': -1}
            sec = sec.replace(m.group(), '')
            for k, pat in (
                    ('points', r'(\d+) Points'),
                    ):
                m = re.search(pat, sec)
                if not m:
                    continue
                chal[k] = m[1]
                sec = re.sub(pat, '', sec)
            for m in re.finditer('<a [^>]*?href="([^"]+)"[^>]*?>.+?</a>', sec, flags=re.S):
                text = clear_tags(m.group())
                if re.search(r'\.(zip|gz)$', text):
                    chal['files'].append({'name': text, 'url': urljoin(OPT.url, m[1])})
                    sec = sec.replace(m.group(), '')
            # clear crap from desc
            sec = re.search('<div class="card-content">(.+)', sec, flags=re.S)[0]
            sec = re.sub(r'<form .+</form>|SOLVED', '', sec, flags=re.S)
            chal['description'] = clear_tags(sec)
            challenges.append(chal)
    return list(categories), challenges


def get_field(jdata, name, default=None, pop=False, crash=True):
    """
    Extract a field from a JSON dict.
    """
    res = None
    alias = None
    for k in [name] + FIELD_ALIAS[name]:
        if k in jdata:
            res = jdata[k]
            alias = k
            break
    if res is None and default is not None:
        res = default
    if res is not None and pop and isinstance(res, list):
        if not res:
            sys.stderr.write(f'[!] Can\'t pop from empty list for field {name=}/{alias=}: {jdata=}!')
            res = 'unknown'
        else:
            res = res[0]
    if res is not None:
        return res
    if crash:
        raise ValueError(f'Field {name} (or its aliases) not found in {jdata}!')


def download_challenges():
    if not OPT.input:
        url = OPT.url + PATHS[OPT.mode]['challenges']
    if OPT.mode == 'ctfx':
        r = download(url)
        categories, cdata = html_to_dict(r.text)
    else:
        if OPT.input:
            cdata = open(OPT.input, encoding='utf8').read().strip()
            if cdata[0] in '{[':
                cdata = json.loads(cdata)
            else:
                raise ValueError(f'Unknown challenge data format in {OPT.input}!')
        else:
            cdata = get_json('challenges')
        if not isinstance(cdata, list):
            cdata = get_field(cdata, 'data')
        categories = [get_field(e, 'category') for e in cdata]
        if isinstance(categories[0], list):
            categories = sum(categories, start=[])
        if isinstance(categories[0], dict):
            categories = [e['name'] for e in categories]
        categories = sorted(set([x.strip('\t ,;') for x in categories]))
    print(f'[*] Categories:', ', '.join(categories))
    jdata_lst = []
    for c in cdata:
        jdata = {}
        jdata_lst.append(jdata)
        meta = get_field(c, 'meta', crash=False)
        if meta and isinstance(meta, dict):
            c.update(meta)
        category = jdata['category'] = get_field(c, 'category', default='no-category', pop=True).strip('\t ,;')
        jdata['categories'] = get_field(c, 'categories', default=[category])
        name = jdata['name'] = get_field(c, 'name', '?')
        if isinstance(name, dict):
            if 'en' in name:
                name = name['en']
            else:
                raise ValueError(f'Unknown field format: {name=}!')
        path = filter_fn(category or 'no-category') + '/' + filter_fn(name)
        if not os.path.isdir(path):
            os.makedirs(path)
        print(f'[-] {category} / {name}')
        id = jdata['id'] = get_field(c, 'id')
        if OPT.mode == 'ctfd':
            c = get_json(f'{PATHS[OPT.mode]["challenges"]}/{id}')['data']
        if 'description' not in c and 'url' in c:
            r = download(urljoin(url, c['url']))
            c['description'] = r.json()['content']['description']
        desc = get_field(c, 'description', '?')
        if isinstance(desc, dict):
            if 'en' in desc:
                desc = desc['en']
            else:
                raise ValueError(f'Unknown field format: {desc=}!')
        if re.search(r'<[a-z]+>', desc, flags=re.I):
            desc = HTML2MD().convert(desc).replace('\u00A0', ' ').replace('\t', '  ').strip()
        desc = re.sub(r'[\r\n][\r\n ]+', '\n\n', desc)
        desc = f'# {name}\n\n{desc}\n'
        if not c.get('connection_info'):
            if 'links' in c and isinstance(c['links'], dict):
                c['connection_info'] = []
                if c['links'].get('nc'):
                    c['connection_info'] += [re.sub('^https?://', 'nc ', e) for e in c['links']['nc']]
                if c['links'].get('web'):
                    c['connection_info'] += c['links']['web']
                c['connection_info'] = '\n'.join(c['connection_info'])
        if 'connection_info' in c:
            if c['connection_info']:
                desc += f'\n{c["connection_info"]}\n'
        elif 'services' in c:
            cinfo = ''
            for s in c['services']:
                if 'user_display' in s:
                    cinfo += f'{s["user_display"]}\n'
                else:
                    sys.stderr.write(f'[!] Missing "user_display" in {s}!\n')
            desc += cinfo
            c['connection_info'] = cinfo
        if c.get('connection_info'):
            jdata['connection'] = c['connection_info']
        if 'hints' in c:
            desc += f'Hints: {str(c.get("hints"))}\n'
        jdata['description'] = desc
        score = jdata['points'] = get_field(c, 'points', -1)
        solves = c.get('solves', '?')
        if isinstance(solves, list):
            solves = len(solves)
        desc += f'\n{solves} solves, score {score} (started from {c.get("initial", "?")}) @ {time.strftime("%H:%M:%S, %d.%m.%Y")}\nID: {id}'
        if OPT.verbose:
            print(f'[#] Saving README for {category}/{name}...')
        open(f'{path}/README.TXT', 'w', encoding='utf8').write(desc)
        if c.get('view'):
            open(f'{path}/README.HTML', 'w', encoding='utf8').write(c['view'])
        furls = get_field(c, 'files', crash=False)
        if not furls and 'links' in c and isinstance(c['links'], dict) and 'static' in c['links']:
            furls = c['links']['static']
        if not furls:
            rx = re.compile(r'\.(pcap(ng)?|tgz|txz|gz|xz|7z|zip|rar|txt|pdf|jpe?g|png|gif|wav|mp3|avi|mp4|mpg)$')
            for m in re.findall(r'\[([^\]]+)\]\(([^)]+)\)', desc):
                if (mext := rx.search(m[0])) or (mext := rx.search(m[1])):
                    furls.append({'name': m[0], 'url': m[1], 'ext': mext.group()})
                elif gurl := re.search(r'https://drive.google.com/file/d/([^/]+)/view', m[1]):
                    furls.append({'name': m[0],
                            'url': f'https://drive.usercontent.google.com/download?id={gurl.groups()[0]}&export=download&confirm=t'})
        if furls:
            jdata['files'] = furls
        for f in furls:
            if isinstance(f, dict):
                fn = f['name']
                furl = f['url']
            else:
                fn = os.path.basename(f)
                if '?' in fn:
                    fn = fn.split('?')[0]
                furl, f = f, {}
            if not (furl.startswith('http') or OPT.url):
                sys.stderr.write(f'[!] Relative-URL found ({furl}), please provide a base URL with -u ...!\n')
                continue
            if f.get('ext') and not fn.endswith(f['ext']):
                fn += '.' + f['ext'].strip('.')
            if not furl.startswith('http'):
                furl = urljoin(OPT.url, furl)
            fn = filter_fn(fn)  # always do this for each file :)
            dest = f'{path}/{fn}'
            if os.path.isfile(dest) and os.path.getsize(dest):
                print(f'[~] File {dest} already exists, skipping...')
                continue
            size = '?'
            try:
                info = get_download_info(furl)
                size = info['size']
                if size > OPT.max_size:
                    sys.stderr.write(f'[!] Skipping download of {fn} / {furl}: too big ({size / (1024 * 1024):.2f}MB)!\n')
                    continue
                if info['name']:
                    nfn = filter_fn(info['name'])
                    if nfn != fn:
                        fn = nfn
                        print(f'[~] Got new filename: {fn}...')
                        dest = f'{path}/{fn}'
                        if os.path.isfile(dest):
                            print(f'[~] File {dest} already exists, skipping...')
                            continue
            except Exception as e:
                sys.stderr.write(f'[!] Failed getting URL info for {fn} / {furl}: {e}!\n')
            try:
                # note: don't use requests, for some reason it breaks the download
                print(f'[-] Downloading {fn} from {furl} ({size} bytes)...')
                r = urlopen(Request(furl, headers={'User-Agent': OPT.user_agent}), timeout=10)
                # rctf doesn't accept downloads from Python's UA for some reason - yields 403
                data = r.read()     # fixme: do it chunked (some files are BIG)
            except (http.client.IncompleteRead, requests.exceptions.ChunkedEncodingError) as e:
                print(f'[!] Incompete read of {fn}!')
                data = b''
                fn += '.broken'
            except urllib.error.URLError as e:
                print(f'[!] Failed getting URL {furl}: {e}!')
                data = b''
                fn += '.broken'
            print(f'[-]   Writing {fn}...')
            open(dest, 'wb').write(data)
            del data
    if OPT.save_json:
        json.dump(jdata_lst, open('challenges.json', 'w'), indent=2, sort_keys=1)


def get_info(what):
    if OPT.mode == 'ctfx':
        if what != 'team_info':
            raise NotImplementedError(f'Not implemented: {what} for {OPT.mode} backends!')
        r = download(OPT.url + PATHS[OPT.mode]['team_info'])
        url = urljoin(OPT.url, re.search(r'<a [^>]*href="(user\?id=\d+)">View public profile</a>', r.text)[1])
        setattr(OPT, 'team_id', url.rsplit('?', 1)[-1])
        r = download(url, nocache=True)
        title, res, fields = 'Team Info', {}, None
        for name, pat in (
                ('points', r'<b>(\d+) Points</b>'),
                ('name', r'<div class="category-name">([^<]+)</div>'),
                ):
            res[name] = re.search(pat, r.text)[1]
        r = download(urljoin(OPT.url, PATHS[OPT.mode]['scoreboard']), nocache=True)
        res['place'] = re.search(r'<div style="margin-right:4px">(\d+)\.</div>\s*<a [^>]+>' + re.escape(res['name']) + '</a>', r.text)[1]
    else:
        res = get_json(what)
        title, fields = INFO.get(what, (what, None))
        if not res.get('data'):
            print(f'[!] No {title}!')
            return
        if OPT.mode == 'watflag':
            if what == 'team_info':
                res = res['team']
            elif what == 'user_info':
                res = res['user']
        else:
            res = res['data']
    print(f'[*] {title}:')
    for k, v in sorted(res.items()):
        if not fields or k in fields:
            print(f'  {k}: {v}')
    if what == 'team_info' and OPT.mode == 'ctfd':
        setattr(OPT, 'team_id', str(res['id']))
        get_info('team_pub_info')
    return res



class HTML2MD(HTMLParser):

    def handle_starttag(self, tag, attrs):
        if tag == 'br':
            self.result.append('\n')
        elif tag == 'ul':
            self.result.append('\n')
        elif tag == 'li':
            self.result.append('- ')
        elif tag == 'pre':
            self.result.append('\n```\n')
        elif tag == 'code':
            self.result.append('`')
        elif tag == 'a':
            self.href = dict(attrs)['href']
            self.result.append('[')

    def handle_endtag(self, tag):
        if tag == 'ul':
            self.result.append('\n')
        elif tag == 'pre':
            self.result.append('\n```\n')
        elif tag == 'code':
            self.result.append('`')
        elif tag == 'p':
            self.result.append('\n\n')
        elif tag == 'a':
            self.result.append(f']({self.href})')

    def handle_data(self, data):
        self.result.append(data)

    def convert(self, html):
        self.result = []
        self.feed(html)
        return ''.join(self.result)



class FieldAliasAdd(argparse.Action):

    def __call__(self, argparser=None, namespace=None, value=None, argname=None, help=None):
        if value:
            k, v = value.split('=')
            if k not in FIELD_ALIAS:
                raise ValueError(f'Unknown field: {k}!')
            FIELD_ALIAS[k].append(v)



parser = argparse.ArgumentParser(description=__doc__ + 'Version: ' + __VER__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-a', '--authfile', help='Auth file: URL on the first line, API token on the second')
parser.add_argument('-c', '--check-token', help='Check token', action='store_true')
parser.add_argument('-k', '--insecure', help='Don\'t check TLS', action='store_true')
parser.add_argument('-ti', '--team-info', help='Get team info', action='append_const', dest='get_info', const='team_info')
parser.add_argument('-ui', '--user-info', help='Get user info', action='append_const', dest='get_info', const='user_info')
parser.add_argument('-aw', '--awards', help='Get award list', action='append_const', dest='get_info', const='awards')
parser.add_argument('-no', '--notifications', help='Get notifications', action='append_const', dest='get_info', const='notifications')
parser.add_argument('-su', '--submissions', help='Get submissions', action='append_const', dest='get_info', const='submissions')
parser.add_argument('-dc', '--download-challenges', help='Download challenges', action='store_true')
parser.add_argument('-m', '--mode', help='Operation mode (usually CTF server type)', default=None,
        choices=('rctf', 'ctfd', 'ctfx', 'watflag', 'custom'))
parser.add_argument('-i', '--input', help='Input file (JSON for -dc)')
parser.add_argument('-o', '--output', help='Output folder')
parser.add_argument('-nc', '--no-cache', help='Disable HTTP caching entirely', action='store_true')
parser.add_argument('-u', '--url', help='Base URL')
parser.add_argument('-A', '--user-agent', help='User agent', default='io-CTF-automator 1.0')
parser.add_argument('-H', '--headers', help='Headers to add to the request', action='append', default=[])
parser.add_argument('-t', '--token', help='Token')
parser.add_argument('-th', '--token-header', help='Token header')
parser.add_argument('-sj', '--save-json', help='Save as JSON (e.g. for -dc)', action='store_true')
parser.add_argument('-fa', '--field-alias', help='Add a JSON-input field alias (e.g. "score=sc0re")', action=FieldAliasAdd)
parser.add_argument('-M', '--max-size', help='Max file size to download for -dc', type=int, default=10 * 1024 * 1024)
parser.add_argument('-v', '--verbose', help='Verbose messages & debug info', action='count', default=0)
OPT = parser.parse_args()

if OPT.insecure:
    import urllib3
    urllib3.disable_warnings()
    ssl._create_default_https_context = ssl._create_unverified_context
if not OPT.input:
    if not OPT.authfile:
        if os.path.isfile('.auth'):
            OPT.authfile = '.auth'
        elif os.path.isfile('.ioctf'):
            for k, v in ast.literal_eval(open('.ioctf').read()).items():
                setattr(OPT, k, v)
    if isinstance(OPT.headers, str):
        OPT.headers = [OPT.headers]
    OPT.headers = dict(tuple(e.split(': ')) for e in OPT.headers)
    if OPT.authfile:
        OPT.url, OPT.token = tuple(line.strip() for line in re.split('[\r\n]+', open(OPT.authfile).read()) if line.strip())
    if hasattr(OPT, 'paths'):
        OPT.mode = 'custom'
        PATHS['custom'] = OPT.paths
    if OPT.url:
        OPT.url = OPT.url.rstrip('/ \t')
    # if OPT.mode == 'guess': ...
    if OPT.token and not OPT.mode:
        if OPT.token.startswith('Bearer '):
            OPT.mode = 'rctf'
        elif OPT.token.startswith('csrftoken='):
            OPT.mode = 'watflag'
        elif OPT.token.startswith('PHPSESSID='):
            OPT.mode = 'ctfx'
        else:
            OPT.mode = 'ctfd'
    if not OPT.token_header:
        if re.search(r'^[a-zA-Z-]+: \S+', OPT.token):
            OPT.token, OPT.token_header = OPT.token.split(': ', 1)
        else:
            OPT.token_header = 'Authorization' if OPT.mode in ('ctfd', 'rctf') else 'Cookie'
    if OPT.mode == 'ctfd' and OPT.token_header == 'Authorization' and OPT.token.startswith('ctfd_'):
        OPT.token = 'Token ' + OPT.token
    if not (OPT.mode and OPT.url and OPT.token):
        sys.exit('Either place the URL and API token on separate lines in an .auth file in the current folder or pass that file to -a!\nRun with -h for details.')
    if 'User-Agent' not in OPT.headers:
        OPT.headers['User-Agent'] = OPT.user_agent
    OPT.headers[OPT.token_header] = OPT.token
    if OPT.verbose:
        print(f'[#] Mode: {OPT.mode}, Token: {OPT.token}, Header: {OPT.token_header}')
    if OPT.check_token:
        check_token()
    if OPT.get_info:
        info = {}
        for k in OPT.get_info:
            if OPT.mode == 'rctf' and k != 'team_info':
                print(f'[!] No {k} for rctf-hosted CTFs!')
                continue
            title = k.replace('_', ' ')
            info[k] = get_info(k)
if OPT.download_challenges:
    download_challenges()

#!/usr/bin/env python3
"""
Create a docset from a mirrored reference website.

Common .docset entry types: Command, Directive, Function, Guide, Macro, Method, Module, Namespace,
Property, Type, Variable, cl (cl == Class).
See https://kapeli.com/docsets#supportedentrytypes for full list.

Pillow example:

make_docset en\stable -t "Pillow" -V "11.0.0" -nr "og:title. content=.(\w+)" -tr "og:title. content=.\w+ (?P<Module>Module)" -sr "(?<=<body>).+?(?=<article)|<footer>.+?(?=</body>)|<(script|style).+?</\1>|<(meta|iframe|link)[^>]+>" -er "<dl class=.py attribute.>\s*<dt class=.sig sig-object py. id=.(?P<anchor>PIL.(?P<Attribute>[\w\.]+)).>" -er "<dl class=.py function.>\s*<dt class=.sig sig-object py. id=.(?P<anchor>PIL.(?P<Function>[\w\.]+)).>" -er "<dl class=.py method.>\s*<dt class=.sig sig-object py. id=.(?P<anchor>PIL.(?P<Method>[\w\.]+)).>"

Author: Vlad Topan (vtopan/gmail)
"""

import argparse
import json
import os
import re
import sqlite3
import sys
import tempfile
import time
import zipfile


__VER__ = '0.1.0.20241024'

PLIST_TEMPLATE = '''<?xml version="1.0" encoding="UTF-8"?>
<plist version="1.0">
    <dict>
        <key>CFBundleIdentifier</key> <string>{filtered_name}/string>
        <key>CFBundleName</key> <string>{doc_name}</string>
        <key>DocSetPlatformFamily</key> <string>{filtered_name}</string>
        <key>isDashDocset</key> <true/>
        <key>isJavaScriptEnabled</key> <false/>
        <key>DashDocSetFamily</key> <string>unsorteddashtoc</string>
        <key>dashIndexFilePath</key> <string>index.html</string>
    </dict>
</plist>
'''

NODES_XML_TEMPLATE = '''<?xml version="1.0" encoding="UTF-8"?>
<DocSetNodes version="1.0">
<TOC>
<Node type="folder">
<Name>Documentation</Name>
<Path>index.html</Path>
</Node>
</TOC>
</DocSetNodes>
'''

def extract_titles(path, type_regex, name_regex=None, extensions=('htm', 'html'), verbose=0):
    """
    Extract titles and guess entry types from HTML files in folder.

    :param type_regex: Regular expression to extract the type (use named groups)
    :param name_regex: Regular expression to extract the name (default: contents of <title> tag)
    :param verbose: How verbose to be (1=print matching filenames, 2=print matching title regex).
    :return: Yields (filename, name, type) tuples.
    """
    if not name_regex:
        name_regex = re.compile(r'<title>([^<]+)</title>', flags=re.I)
    for root, dirs, files in os.walk(path):
        if verbose > 2:
            sys.stderr.write(f'[*] Entering {root}...\n')
        for basename in files:
            filename = os.path.join(root, basename)
            if basename.split('.', 1)[-1].lower() in extensions:
                if verbose:
                    sys.stderr.write(f'[*] Found filename {filename}...\n')
                text = open(filename, encoding='utf8').read()
                if not (name := name_regex.search(text)):
                    continue
                name = name.group(1)
                if verbose > 1:
                    sys.stderr.write(f'[-]   Found {name=}...\n')
                if m := type_regex.search(text):
                    for etype, value in m.groupdict().items():
                        if value is not None:
                            yield (filename, name, etype)
                            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='MakeDocset',
            description=r'''Creates a Dash/Zeal .docset (as Zip) from an already-mirrored website.

Examples

PySide6:

wget https://doc.qt.io/qtforpython-6/ -nH -c -m -np -L -k \
--reject="*.css*,*.mp4,*.avi,*.m4v,*.mobi" -e robots=off --no-check-certificate \
--compression=auto --cut-dirs=0 -e robots=off

./make_docset.py qtforpython-6 -t PySide6 -nr "internal. href=.(\w+).html#.>" \
-tr "(?P<Module><dl class=.py module)|(?P<cl><dl class=.py class)" \
-sr "(?<=<body>).+?(?=<h1>)|<footer>.+?(?=</body>)|<(script|style)[^>]*>.+?</\1>\
|<(meta|iframe|link)[^>]+>"

Pillow:

make_docset en\stable -t "Pillow" -V "11.0.0" -nr "og:title. content=.(\w+)" \
-tr "og:title. content=.\w+ (?P<Module>Module)" \
-sr "(?<=<body>).+?(?=<article)|<footer>.+?(?=</body>)|<(script|style).+?</\1>\
|<(meta|iframe|link)[^>]+>"
-er "<dl class=.py attribute.>\s*<dt class=.sig sig-object py. id=.(?P<anchor>PIL.(?P<Attribute>[\w\.]+)).>" \
-er "<dl class=.py function.>\s*<dt class=.sig sig-object py. id=.(?P<anchor>PIL.(?P<Function>[\w\.]+)).>" \
-er "<dl class=.py method.>\s*<dt class=.sig sig-object py. id=.(?P<anchor>PIL.(?P<Method>[\w\.]+)).>"
''', formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('folder', help='mirrored folder path')
    parser.add_argument('-t', '--title', help='What is documented', required=True)
    parser.add_argument('-V', '--version', help='Version of documented thing')
    parser.add_argument('-o', '--output', help='output zip file name (default: <title>.zip)')
    parser.add_argument('-e', '--extensions',
            help='comma-separated file extensions to parse', default='htm,html')
    parser.add_argument('-nr', '--name-regex',
            help='regex pattern to extract entry name (instead of using the <title> tag)')
    parser.add_argument('-tr', '--type-regex',
            help='regex pattern to extract type - use named groups',
            default='', required=True)
    parser.add_argument('-fr', '--filter-regex',
            help='regex pattern to remove from titles when generating index')
    parser.add_argument('-sr', '--strip-regex', help='regex pattern to strip from each page',
            default=r'<(script|style).+?</\1>|<(meta|iframe|link)[^>+]>')
    parser.add_argument('-er', '--entry-regex', action='append', default=[],
            help='regex to extract extra fields from the page (must be named as the type)')
    parser.add_argument('-v', '--verbose', help='Print debug info to stderr', action='count',
            default=0)
    opt = parser.parse_args()
    # process arguments
    rx = {name: re.compile(getattr(opt, name + '_regex'), flags=re.S)
            if getattr(opt, name + '_regex') else None
            for name in ('name', 'type', 'filter', 'strip')}
    rx['entries'] = [re.compile(e, flags=re.S) for e in opt.entry_regex] if opt.entry_regex else []
    opt.extensions = [e.strip().lower() for e in opt.extensions.split(',')]
    doc_name = opt.title
    filtered_name = re.sub('[^a-z0-9]+', '_', doc_name, flags=re.I).lower()
    folder_name = re.sub('[^a-z0-9+-]+', '_', doc_name, flags=re.I) + '.docset'
    if not opt.output:
        opt.output = f'{folder_name}.zip'
    # prepare files
    dbf = tempfile.NamedTemporaryFile(delete_on_close=False)
    dbf.close()
    zf = zipfile.ZipFile(opt.output, 'w', compression=zipfile.ZIP_DEFLATED)
    dbc = sqlite3.connect(dbf.name)
    cur = dbc.cursor()
    cur.execute('CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, '
            'path TEXT);')
    cur.execute('CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path);')
    # walk documents
    for filename, name, etype in extract_titles(opt.folder, rx['type'], rx['name'],
            extensions=opt.extensions, verbose=opt.verbose):
        if rx['filter']:
            name = rx['filter'].sub('', name)
        if opt.verbose:
            sys.stderr.write(f'[-]   Found {name}:{etype} in {filename}...\n')
        filename = filename.replace("\\", "/").lstrip('./')
        cur.execute('INSERT INTO searchIndex(name, type, path) VALUES (?, ?, ?)''',
                (name, etype, filename))
        text = open(filename, encoding='utf8').read()
        if rx['strip']:
            text = rx['strip'].sub('', text)
        if 'meta charset' not in text:
            text = text.replace('</head>', '<meta charset="UTF-8"></head>')
        zf.writestr(f'{folder_name}/Contents/Resources/Documents/{filename}', text)
        for regex in rx['entries']:
            for m in regex.finditer(text):
                anchor = None
                for etype, name in m.groupdict().items():
                    if etype == 'anchor':
                        anchor = name
                        continue
                    if name is not None:
                        if opt.verbose:
                            sys.stderr.write(f'[-]   Found {name}:{etype} in {filename}...\n')
                        path = filename
                        if anchor:
                            path += '#' + anchor
                        cur.execute('INSERT INTO searchIndex(name, type, path) VALUES (?, ?, ?)''',
                                (name, etype, path))
    cur.execute('COMMIT')
    cur.close()
    dbc.close()
    # write metadata
    zf.write(dbf.name, f'{folder_name}/Contents/Resources/docSet.dsidx')
    zf.writestr(f'{folder_name}/Contents/Info.plist', PLIST_TEMPLATE.format(**locals()))
    meta = {'name': doc_name, 'title': doc_name}
    if opt.version:
        meta['version'] = opt.version
    zf.writestr(f'{folder_name}/meta.json', json.dumps(meta))
    zf.writestr(f'{folder_name}/Contents/Resources/Nodes.xml', NODES_XML_TEMPLATE)
    zf.writestr(f'{folder_name}/Contents/Resources/Documents/index.html',
            f'<html><head><title>{opt.title}</title></head><body><h1>{opt.title}</h1>'
            f'<p>Documentation for <b>{opt.title}</b> generated by '
            f'{os.path.basename(sys.argv[0])} {__VER__} '
            f'@ {time.strftime("%H:%M:%S, %d.%m.%Y")}.</p></body></html>')
    zf.close()
    # done
    sys.stderr.write(f'[*] Created {opt.output}.\n')

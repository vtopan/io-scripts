#!/usr/bin/env python3
"""
Download presentations from SlideShare as PDFs (slides are JPG images).

Inspired by https://github.com/cballenar/slideshare-to-pdf/blob/master/grub.py

Author: Vlad Topan (vtopan/gmail)
"""

import io
import re
import sys

try:
    from bs4 import BeautifulSoup 
except ImportError:
    sys.exit('[!] BeautifulSoup module missing, run `pip3 install bs4`!')
try:
    from PIL import Image
except ImportError:
    sys.exit('[!] Pillow module missing, run `pip3 install pillow`!')
try:
    from iolib.net import download
except ImportError:
    try:
        import requests
        download = lambda url:requests.get(url).content
    except ImportError:
        sys.exit('[!] requests module missing, run `pip3 install requests`!')


if len(sys.argv) < 2:
    sys.exit(f'Usage: {sys.argv[0]} <URL> [<output-filename>]')
url = sys.argv[1]
outfile = sys.argv[2] if len(sys.argv) > 2 else None
print(f'[*] Downloading [{url}]...') 
html = download(url).decode('utf8')
soup = BeautifulSoup(html, 'lxml')
images = soup.find_all('img', attrs={'class': 'slide_image'})
key = 'data-full' if images[0].has_attr('data-full') else 'data-normal'
if not outfile:
    outfile = re.sub(r'[^\w()[\]-]+', ' ', soup.find_all('title')[0].string.replace(': ', ' - '))[:64].strip('_ ') + '.pdf'
print(f'[#] Using attribute {key}...')
img_lst = []
for i, img_tag in enumerate(images, 1):
    img_url = img_tag[key]
    print(f'[*] Downloading [{img_url}]...')
    data = download(img_url)
    img = Image.open(fp=io.BytesIO(data))
    img_lst.append(img)
print(f'[*] Saving {outfile}...')
img_lst[0].save(outfile, 'PDF', resolution=100.0, save_all=True, append_images=img_lst[1:])

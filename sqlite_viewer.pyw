#!/usr/bin/env python3
"""
SQLite DB viewer.

Author: Vlad Topan (vtopan/gmail)
"""
from libioqt import create_layouts, VLabelEdit

from PySide6.QtWidgets import QMainWindow, QApplication, QMessageBox, QHBoxLayout, QVBoxLayout, \
        QPushButton, QWidget, QTextBrowser, QLabel, QLineEdit, QStatusBar, QTabWidget, \
        QFrame, QComboBox, QCompleter, QTableWidget, QTableWidgetItem, QDateEdit, QDateTimeEdit, \
        QCheckBox, QMessageBox, QTreeWidget, QTreeWidgetItem, QHeaderView, QGridLayout, QTextEdit, \
        QStyleFactory, QFileDialog
from PySide6.QtCore import Qt, QCoreApplication, Signal, QThread, QByteArray, SIGNAL, Slot, \
        Signal, QDate, QDateTime, QEvent, QSortFilterProxyModel
from PySide6.QtGui import QClipboard, QFont, QIcon, QFontMetrics, QKeySequence, QDoubleValidator, \
        QDesktopServices, QColor, QPalette, QBrush, QPixmap, QTextCursor, QShortcut

import os
import functools
import sys
import threading
import time
import traceback

from iogp import AttrDict
from iogp.db import SqliteDB


__VER__ = '0.1.0.20220101'

APP_PATH = os.path.dirname(__file__)
MAINWINDOW = None


def excepthook(exctype, value, traceback):
    sys.stderr.write(f'[!] {exctype}: {value}\n{traceback}\n')
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)
sys._excepthook = sys.excepthook
sys.excepthook = excepthook


def run_in_main(fun):
    @functools.wraps(fun)
    def _(*args, **kwargs):
        MAINWINDOW.rim(fun, args, kwargs)
    return _



class MainWindow(QMainWindow):

    set_ui_text_sig = Signal(str, str)
    run_in_main_sig = Signal(object, object, object)


    def __init__(self, app):
        global MAINWINDOW
        super().__init__()
        MAINWINDOW = self
        self.eventlog = None
        self.log_lock = threading.Lock()
        self.stop = 0
        self.setWindowTitle('SQLite Viewer')
        self.clipboard = QApplication.clipboard()
        self.app = app
        self.fonts = {
            'fixed': QFont('Terminal', 8)
            }
        #self.setWindowIcon(QIcon(f'{APP_PATH}/....png'))
        self.init_ui_elements()
        self.init_shortcuts()
        self.setCentralWidget(self.main)
        self.log('Hello, world! Press Esc to quit.')
        self.db = None
        self.wmap.output.setMinimumSize(800, 600)
        if len(sys.argv) > 1:
            self.wmap.filename.setText(sys.argv[1])
            if os.path.isfile(sys.argv[1]):
                self.clicked_open()
                if self.wmap.tables.currentText():
                    self.clicked_dump()
        self.wmap.tables.currentTextChanged.connect(self.clicked_dump)
        self.wmap.query.edit.returnPressed.connect(self.clicked_execute)


    def init_shortcuts(self):
        """
        Initialize keyboard shortcuts.
        """
        QShortcut(QKeySequence("Esc"), self).activated.connect(self.quit)


    def clicked_browse(self):
        filename, _ = QFileDialog.getOpenFileName(None, '', '', 'All Files (*);;DB Files (*.db;*.sqlite)')
        self.wmap.filename.setText(filename)


    def clicked_open(self):
        filename = self.wmap.filename.text().strip()
        self.db = SqliteDB(filename)
        tables = [x[0] for x in self.db.fetchall("SELECT name FROM sqlite_master WHERE type = 'table'")]
        self.wmap.tables.addItems(tables)


    def clicked_dump(self, ignore=None):
        table = self.wmap.tables.currentText().strip()
        if not table:
            self.wmap.query.setText('')
            self.wmap.output.setText('')
            return
        self.wmap.query.setText(f'SELECT * FROM {table}')
        rows = self.db.fetchone(f'SELECT count(1) FROM {table}')[0]
        data = self.db.fetchall(f'SELECT * FROM {table} LIMIT 100')
        lst = ['<tr><td><b>' + '</b></td><td><b>'.join(self.db.get_table_columns(table)) + '</b></td></tr>']
        for row in data:
            lst.append('<tr><td>' + '</td><td>'.join(str(e).replace('<', '&lt;') for e in row) + '</td></tr>')
        style = '<style>td {border: 1px solid black; padding: 2px;margin: 0px;}\ntr, table {border: 0px;padding: 0px; margin: 0px;}</style>'
        self.wmap.output.setText(style + f'<p>Table <b>{table}</b> has {rows} rows.</p><table>{"".join(lst)}</table>')


    def clicked_execute(self):
        query = self.wmap.query.text()
        if 'limit ' not in query.lower():
            query += ' LIMIT 100'
            self.wmap.query.setText(query)
        try:
            data = self.db.fetchall(query)
        except Exception as e:
            self.exc('Query failed!')
            return
        lst = ['<tr><td><b>' + '</b></td><td><b>'.join([e[0] for e in self.db.cur.description]) + '</b></td></tr>']
        for row in data:
            lst.append('<tr><td>' + '</td><td>'.join(str(e).replace('<', '&lt;') for e in row) + '</td></tr>')
        style = '<style>td {border: 1px solid black; padding: 2px;margin: 0px;}\ntr, table {border: 0px;padding: 0px; margin: 0px;}</style>'
        self.wmap.output.setText(style + f'<p>Query produced {len(lst) - 1} rows.</p><table>{"".join(lst)}</table>')


    def init_ui_elements(self):
        """
        Initialize UI elements (widgets + layouts) and add them to self.wmap.
        """
        self.setFocusPolicy(Qt.StrongFocus)
        self.run_in_main_sig.connect(self.runner_slot)
        self.fieldmap = {}
        self.main = QWidget()
        widgets = [
            {
                'filename': VLabelEdit('Filename:'),
                'browse': QPushButton('Browse'),
                'open': QPushButton('Open'),
            },
            {
                'tables': QComboBox(),
                'dump': QPushButton('Dump'),
            },
            {
                'query': VLabelEdit('Query:'),
                'execute': QPushButton('Execute'),
            },
            {
                'output': QTextBrowser(),
            },
            {'eventlog': QTextBrowser()},
        ]
        self.lay, self.wmap = create_layouts(widgets)
        self.wmap = AttrDict(self.wmap)
        self.main.setLayout(self.lay)
        # set up event log
        self.eventlog = self.wmap.eventlog
        self.eventlog.setStyleSheet('QScrollBar:vertical {width: 10px;}')
        self.eventlog.setPlaceholderText('Log messages')
        self.eventlog.setReadOnly(1)
        rowheight = QFontMetrics(self.eventlog.font()).lineSpacing()
        self.eventlog.setFixedHeight(10 + 4 * rowheight)
        self.eventlog.setOpenExternalLinks(True)
        self.eventlog.setFocus()
        # auto-connect buttons to clicked_* methods
        for k, v in self.wmap.items():
            if isinstance(v, QPushButton):
                method = 'clicked_' + k
                if hasattr(self, method):
                    self.wmap[k].clicked.connect(getattr(self, method))
                else:
                    sys.stderr.write(f'Missing implementation for {method}!\n')
        self.set_ui_text_sig.connect(self.set_ui_text_slot)


    def rim(self, fun, args=None, kwargs=None):
        """
        Run-in-main - run a function (or a QMainWindow method) in the main UI thread.
        """
        self.run_in_main_sig.emit(fun, args or [], kwargs or {})


    @Slot(object, object, object)
    def runner_slot(self, fun, args, kwargs):
        """
        Run-in-main slot.
        """
        if type(fun) is str:
            fun = getattr(self, fun)
        try:
            fun(*args, **kwargs)
        except Exception as e:
            self.exc(f'Crashed running {fun} in the main thread: {e}')


    @Slot(str, str)
    def set_ui_text_slot(self, node, value):
        """
        Slot for the .set_ui_text_sig() signal.
        """
        if node == 'eventlog':
            return self._log(value)
        raise ValueError(f'Unknown node {node}!')


    def _log(self, msg):
        """
        Local log - only to be called from the main UI thread.
        """
        print(f'[UI] {msg}')
        if not self.eventlog:
            return
        self.log_lock.acquire()
        if msg.startswith('[DEBUG]'):
            msg = f'<font color="#444">{msg}</font>'
        elif msg.startswith('[ERROR]') or msg.startswith('[CRASH]'):
            msg = f'<font color="#A00">{msg}</font>'
        msg = f'<font color="#777">{time.strftime("[%d.%m.%y %H:%M:%S]")}</font> {msg}'
        self.eventlog.append(msg.replace('\n', '<br>'))
        self.eventlog.ensureCursorVisible()
        self.log_lock.release()


    def set_ui_text(self, node, value):
        """
        Allows setting UI elements from other threads (signal/slot mechanism).
        """
        value = str(value)
        self.set_ui_text_sig.emit(node, value)


    def log(self, msg):
        """
        Add message to the log.
        """
        self.set_ui_text('eventlog', msg)


    def dbg(self, msg):
        """
        Add a debug message to the log.
        """
        self.set_ui_text('eventlog', '[DEBUG] ' + msg)


    def err(self, msg):
        """
        Add an error message to the log.
        """
        self.set_ui_text('eventlog', '[ERROR] ' + msg)


    def exc(self, msg):
        """
        Add an error message to the log.
        """
        self.set_ui_text('eventlog', traceback.format_exc())
        self.set_ui_text('eventlog', f'[CRASH] {msg}')


    def quit(self):
        """
        Close app.
        """
        self.stop = 1
        self.app.quit()



def run(args=sys.argv):
    app = QApplication(args)
    app.setStyle(QStyleFactory.create("Plastique"))
    main = MainWindow(app)
    main.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    try:
        sys.exit(run(sys.argv))
    except Exception as e:
        sys.stderr.write(f'[!] Dead: {e}\n')
        raise